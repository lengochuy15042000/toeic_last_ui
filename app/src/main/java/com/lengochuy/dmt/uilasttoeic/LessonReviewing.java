package com.lengochuy.dmt.uilasttoeic;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.lengochuy.dmt.uilasttoeic.Model.AnswerReviewing;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class LessonReviewing extends AppCompatActivity {
    RecyclerView recyclerViewReviewing;
    LessonReviewingAdapter lessonReviewingAdapter;
    TextView txtNameQuestion,txtNameLesson,txtShowResult,txtShowFinishOn;
    ExtendedFloatingActionButton fab1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson_reviewing);

        initUI();
    }

    private void initUI() {
        txtNameQuestion = findViewById(R.id.txtNameQuestionRv);
        txtNameLesson   = findViewById(R.id.txtNameLessonRv);
        txtShowResult   = findViewById(R.id.txtShowResult);
        txtShowFinishOn = findViewById(R.id.txtShowFinishOn);
        Date date = new Date();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("E, MMM-dd, HH");
        txtShowFinishOn.setText(simpleDateFormat.format(date));

        recyclerViewReviewing   = findViewById(R.id.recyclerViewLessonReviewing);
        lessonReviewingAdapter  = new LessonReviewingAdapter(getListReviewing());
        recyclerViewReviewing.setLayoutManager(new GridLayoutManager(this,5));
        recyclerViewReviewing.setHasFixedSize(true);
        recyclerViewReviewing.setAdapter(lessonReviewingAdapter);

    }

    @SuppressLint("SetTextI18n")
    private List<AnswerReviewing> getListReviewing() {
        List <AnswerReviewing> list = new ArrayList<>();
        int numberQSCorrect = 0;
        Set<Integer> keySet = QuestionAdapter.listAnswer.keySet();
        for (Integer key : keySet){
            Log.d("test2222",key + "-" + QuestionAdapter.listAnswer.get(key));
            if (QuestionAdapter.listAnswer.get(key) != null){
                if (QuestionAdapter.listAnswer.get(key))
                    numberQSCorrect++;
                list.add(new AnswerReviewing(key,QuestionAdapter.listAnswer.get(key)));
            }
        }
        int totalQuestion = MainActivity.numberQuestion;
        txtShowResult.setText(numberQSCorrect + "/" + totalQuestion);
        return list;
    }
}