package com.lengochuy.dmt.uilasttoeic;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.lengochuy.dmt.uilasttoeic.Model.AnswerReviewing;

import java.util.List;

public class LessonReviewingAdapter extends RecyclerView.Adapter<LessonReviewingAdapter.LessonReviewingViewHolder>{
    List<AnswerReviewing> list;

    public LessonReviewingAdapter(List<AnswerReviewing> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public LessonReviewingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_show_answer, parent, false);
        return new LessonReviewingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LessonReviewingViewHolder holder, int position) {
        AnswerReviewing answerReviewing = list.get(position);
        if (answerReviewing == null){
            return;
        }
        if (answerReviewing.isTrue()){
            holder.cardViewRv.setCardBackgroundColor(Color.RED);
        }else{
            holder.cardViewRv.setCardBackgroundColor(Color.GREEN);
        }
        holder.textViewRv.setText(answerReviewing.getIndexAnswer() + "");
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class LessonReviewingViewHolder extends RecyclerView.ViewHolder{
        CardView cardViewRv;
        TextView textViewRv;
        public LessonReviewingViewHolder(@NonNull View itemView) {
            super(itemView);

            cardViewRv  = itemView.findViewById(R.id.background_answer);
            textViewRv  = itemView.findViewById(R.id.textViewRv);
        }
    }
}
