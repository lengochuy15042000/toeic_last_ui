package com.lengochuy.dmt.uilasttoeic;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;

import com.lengochuy.dmt.uilasttoeic.Model.QuestionLesson;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.QuestionViewHolder>{
    static List<QuestionLesson> questionLessonList;
    Context context;
    public static Map<Integer, Boolean> listAnswer = new HashMap<>();

    public QuestionAdapter(List<QuestionLesson> questionLessonList, Context context) {
        QuestionAdapter.questionLessonList = questionLessonList;
        this.context = context;
    }

    @NonNull
    @Override
    public QuestionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.layout_item_question_simple_type1, parent, false);
        return new QuestionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull QuestionViewHolder holder, int position) {
        QuestionLesson questionLesson = questionLessonList.get(position);
        if (questionLesson.getTextQuestion() == null){
            holder.img_Question.setImageResource(questionLesson.getResourceImage());
            holder.txtQuestion.setVisibility(View.INVISIBLE);
        }else{
            holder.img_Question.setVisibility(View.INVISIBLE);
            holder.txtQuestion.setVisibility(View.VISIBLE);
            holder.txtQuestion.setText(questionLesson.getTextQuestion());
        }
        holder.buttonAnswer1.setText(questionLesson.getAnswersList().get(0).getLabel());
        holder.buttonAnswer2.setText(questionLesson.getAnswersList().get(1).getLabel());
        holder.buttonAnswer3.setText(questionLesson.getAnswersList().get(2).getLabel());
        holder.buttonAnswer4.setText(questionLesson.getAnswersList().get(3).getLabel());

        holder.txtAnswer1.setText(questionLesson.getAnswersList().get(0).getContentAnswer());
        holder.txtAnswer2.setText(questionLesson.getAnswersList().get(1).getContentAnswer());
        holder.txtAnswer3.setText(questionLesson.getAnswersList().get(2).getContentAnswer());
        holder.txtAnswer4.setText(questionLesson.getAnswersList().get(3).getContentAnswer());

        //Xử lý trình phát nhạc
        MediaPlayer mediaPlayer = MediaPlayer.create(context,questionLesson.getResourceSound());

        //Xử lý khi nhấn vào nút play
        holder.ic_play.setOnClickListener(v -> {
            if (mediaPlayer.isPlaying()){
                holder.ic_play.setImageResource(R.drawable.ic_play);
                mediaPlayer.pause();
            }else{
                holder.ic_play.setImageResource(R.drawable.ic_pause);
                mediaPlayer.start();
            }


            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mm:ss");
            holder.txtTotalTime.setText(simpleDateFormat.format(mediaPlayer.getDuration()));

            //Gán max của seekBar bằng tổng time của mediaPlayer
            holder.seekBar.setMax(mediaPlayer.getDuration());

            //Load seekbar
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    @SuppressLint("SimpleDateFormat")
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mm:ss");
                    holder.txtCurrentTime.setText(simpleDateFormat.format(mediaPlayer.getCurrentPosition()));

                    //Update seekBar
                    holder.seekBar.setProgress(mediaPlayer.getCurrentPosition());
                    if (holder.seekBar.getProgress() == holder.seekBar.getMax()){
                        holder.ic_play.setImageResource(R.drawable.ic_play);
                        holder.seekBar.setProgress(0);
                        mediaPlayer.pause();
                    }

                    handler.postDelayed(this,500);
                }
            },100);
        });

        //Xet su thay doi cua seekBar
        holder.seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mediaPlayer.seekTo(seekBar.getProgress());
            }
        });


    }



    @Override
    public int getItemCount() {
        return questionLessonList.size();
    }

    public static class QuestionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView ic_play, img_Question;
        SeekBar seekBar;
        TextView txtCurrentTime, txtTotalTime, txtQuestion;
        AppCompatButton buttonAnswer1, buttonAnswer2, buttonAnswer3, buttonAnswer4;
        TextView txtAnswer1, txtAnswer2, txtAnswer3, txtAnswer4;
        public QuestionViewHolder(@NonNull View itemView) {
            super(itemView);
            ic_play = itemView.findViewById(R.id.ic_start_music1);
            img_Question    = itemView.findViewById(R.id.imageQuestion1);
            seekBar = itemView.findViewById(R.id.seek_bar1);
            txtCurrentTime  = itemView.findViewById(R.id.textViewCurrentTime1);
            txtTotalTime    = itemView.findViewById(R.id.textViewTotalTime1);
            buttonAnswer1   = itemView.findViewById(R.id.buttonAnswerA1);
            buttonAnswer2   = itemView.findViewById(R.id.buttonAnswerB1);
            buttonAnswer3   = itemView.findViewById(R.id.buttonAnswerC1);
            buttonAnswer4   = itemView.findViewById(R.id.buttonAnswerD1);
            txtAnswer1      = itemView.findViewById(R.id.textViewAnswerA1);
            txtAnswer2      = itemView.findViewById(R.id.textViewAnswerB1);
            txtAnswer3      = itemView.findViewById(R.id.textViewAnswerC1);
            txtAnswer4      = itemView.findViewById(R.id.textViewAnswerD1);

            txtQuestion     = itemView.findViewById(R.id.textViewQuestion1);


            buttonAnswer1.setOnClickListener(this);
            buttonAnswer2.setOnClickListener(this);
            buttonAnswer3.setOnClickListener(this);
            buttonAnswer4.setOnClickListener(this);

        }

        @SuppressLint("NonConstantResourceId")
        @Override
        public void onClick(View v) {
            int kt;
            switch (v.getId()){
                case R.id.buttonAnswerA1:
                    kt = 1;
                    buttonAnswer1.setBackgroundResource(R.drawable.answer_button_background_seleced);
                    buttonAnswer2.setBackgroundResource(R.drawable.answer_button_background_normal);
                    buttonAnswer3.setBackgroundResource(R.drawable.answer_button_background_normal);
                    buttonAnswer4.setBackgroundResource(R.drawable.answer_button_background_normal);
                    break;
                case R.id.buttonAnswerB1:
                    kt = 2;
                    buttonAnswer2.setBackgroundResource(R.drawable.answer_button_background_seleced);
                    buttonAnswer1.setBackgroundResource(R.drawable.answer_button_background_normal);
                    buttonAnswer3.setBackgroundResource(R.drawable.answer_button_background_normal);
                    buttonAnswer4.setBackgroundResource(R.drawable.answer_button_background_normal);
                    break;
                case R.id.buttonAnswerC1:
                    kt = 3;
                    buttonAnswer3.setBackgroundResource(R.drawable.answer_button_background_seleced);
                    buttonAnswer2.setBackgroundResource(R.drawable.answer_button_background_normal);
                    buttonAnswer1.setBackgroundResource(R.drawable.answer_button_background_normal);
                    buttonAnswer4.setBackgroundResource(R.drawable.answer_button_background_normal);
                    break;
                case R.id.buttonAnswerD1:
                    kt = 4;
                    buttonAnswer4.setBackgroundResource(R.drawable.answer_button_background_seleced);
                    buttonAnswer2.setBackgroundResource(R.drawable.answer_button_background_normal);
                    buttonAnswer3.setBackgroundResource(R.drawable.answer_button_background_normal);
                    buttonAnswer1.setBackgroundResource(R.drawable.answer_button_background_normal);
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + v.getId());
            }
            //add answer in hash map.
            listAnswer.put(getAdapterPosition()+1,questionLessonList.get(getAdapterPosition()).getAnswersList().get(kt-1).isTrue());
        }
    }
}
