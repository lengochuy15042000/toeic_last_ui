package com.lengochuy.dmt.uilasttoeic;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.lengochuy.dmt.uilasttoeic.Model.Answers;
import com.lengochuy.dmt.uilasttoeic.Model.QuestionLesson;
import com.shashank.sony.fancygifdialoglib.FancyGifDialog;
import com.shashank.sony.fancygifdialoglib.FancyGifDialogListener;

import java.util.ArrayList;
import java.util.List;

import me.zhanghai.android.fastscroll.FastScroller;
import me.zhanghai.android.fastscroll.FastScrollerBuilder;

public class MainActivity extends AppCompatActivity{
    RecyclerView recyclerViewQuestion;
    QuestionAdapter questionAdapter;
    ExtendedFloatingActionButton fab;
    public static int numberQuestion = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerViewQuestion    = findViewById(R.id.recyclerViewQuestion);
        questionAdapter         = new QuestionAdapter(getListQuestion(),this);
        recyclerViewQuestion.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewQuestion.setHasFixedSize(true);
        recyclerViewQuestion.setAdapter(questionAdapter);
        fab = findViewById(R.id.fab);

        recyclerViewQuestion.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0)
                    fab.extend();
                else fab.shrink();
            }
        });
        fab.setOnClickListener(v -> {
            //Show lên alert để người dùng có đồng ý nộp bài hay ko? (làm sau)
            new FancyGifDialog.Builder(this)
                    .setTitle("Granny eating chocolate dialog box") // You can also send title like R.string.from_resources
                    .setMessage("This is a granny eating chocolate dialog box. This library is used to help you easily create fancy gify dialog.") // or pass like R.string.description_from_resources
                    .setNegativeBtnText("Cancel") // or pass it like android.R.string.cancel
                    .setPositiveBtnBackground(R.color.design_default_color_on_primary) // or pass it like R.color.positiveButton
                    .setPositiveBtnText("Summit") // or pass it like android.R.string.ok
                    .setNegativeBtnBackground(R.color.design_default_color_primary_dark) // or pass it like R.color.negativeButton
                    .setGifResource(R.drawable.giphy)   //Pass your Gif here
                    .isCancellable(false)
                    .OnPositiveClicked(() -> {
                        Intent intent = new Intent(MainActivity.this,LessonReviewing.class);
                        startActivity(intent);
                    })
                    .OnNegativeClicked(() -> Toast.makeText(MainActivity.this,"Cancel",Toast.LENGTH_SHORT).show())
                    .build();
            //Toast.makeText(this, QuestionAdapter.listAnswer.toString(), Toast.LENGTH_SHORT).show();
        });
        new FastScrollerBuilder(recyclerViewQuestion).build();

    }

    private List<QuestionLesson> getListQuestion() {
        List <QuestionLesson> lessons = new ArrayList<>();
        List <Answers> answersList = new ArrayList<>();
        answersList.add(new Answers("A","hdasdhasdaksd",true));
        answersList.add(new Answers("B","hdasdhasdaksd",false));
        answersList.add(new Answers("C","hdasdhasdaksd",false));
        answersList.add(new Answers("D","hdasdhasdaksd",false));
        lessons.add(new QuestionLesson(R.raw.demo,R.drawable.img3,"How many people is your family?"
                ,answersList));

        List <Answers> answersList1 = new ArrayList<>();
        answersList1.add(new Answers("A","1",false));
        answersList1.add(new Answers("B","2",true));
        answersList1.add(new Answers("C","2",false));
        answersList1.add(new Answers("D","3",false));
        lessons.add(new QuestionLesson(R.raw.demo,R.drawable.img3,null
                ,answersList1));

        List <Answers> answersList2 = new ArrayList<>();
        answersList2.add(new Answers("A","This is a house",false));
        answersList2.add(new Answers("B","This is a hometown",false));
        answersList2.add(new Answers("C","This is a village",false));
        answersList2.add(new Answers("D","This is a mountain",true));
        lessons.add(new QuestionLesson(R.raw.demo1,R.drawable.img2,null
                ,answersList2));

        List <Answers> answersList3 = new ArrayList<>();
        answersList3.add(new Answers("A","This is a house",false));
        answersList3.add(new Answers("B","This is a hometown",false));
        answersList3.add(new Answers("C","This is a village",false));
        answersList3.add(new Answers("D","This is a mountain",true));
        lessons.add(new QuestionLesson(R.raw.demo1,R.drawable.img1,null
                ,answersList3));

        List <Answers> answersList4 = new ArrayList<>();
        answersList4.add(new Answers("A","This is a house",false));
        answersList4.add(new Answers("B","This is a hometown",false));
        answersList4.add(new Answers("C","This is a village",false));
        answersList4.add(new Answers("D","This is a mountain",true));
        lessons.add(new QuestionLesson(R.raw.demo1,R.drawable.img1,null
                ,answersList4));

        List <Answers> answersList5 = new ArrayList<>();
        answersList5.add(new Answers("A","This is a house",false));
        answersList5.add(new Answers("B","This is a hometown",false));
        answersList5.add(new Answers("C","This is a village",false));
        answersList5.add(new Answers("D","This is a mountain",true));
        lessons.add(new QuestionLesson(R.raw.demo1,R.drawable.img1,null
                ,answersList5));

        List <Answers> answersList6 = new ArrayList<>();
        answersList6.add(new Answers("A","This is a house",false));
        answersList6.add(new Answers("B","This is a hometown",false));
        answersList6.add(new Answers("C","This is a village",false));
        answersList6.add(new Answers("D","This is a mountain",true));
        lessons.add(new QuestionLesson(R.raw.demo1,R.drawable.img1,null
                ,answersList6));

        List <Answers> answersList7 = new ArrayList<>();
        answersList7.add(new Answers("A","This is a house",false));
        answersList7.add(new Answers("B","This is a hometown",false));
        answersList7.add(new Answers("C","This is a village",false));
        answersList7.add(new Answers("D","This is a mountain",true));
        lessons.add(new QuestionLesson(R.raw.demo1,R.drawable.img1,null
                ,answersList7));

        numberQuestion  = lessons.size();
        return lessons;
    }

}