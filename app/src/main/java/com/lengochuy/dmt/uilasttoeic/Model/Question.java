package com.lengochuy.dmt.uilasttoeic.Model;

import java.util.List;

//@NotNull Integer lessonId, String type, String description,
//        String content, Float mark, String answer, Integer parentId,
//        Integer categoryId, Integer orderInLesson
public class Question {
    private Integer lessonId;
    private String type;
    private String description;
    private Content content;
    private Float mark;
    private String answer;
    private Integer parentId;
    private Integer categoryId;
    private Integer orderInLesson;

    public Question(Integer lessonId, String type, String description, Content content,
                    Float mark, String answer, Integer parentId, Integer categoryId,
                    Integer orderInLesson) {
        this.lessonId = lessonId;
        this.type = type;
        this.description = description;
        this.content = content;
        this.mark = mark;
        this.answer = answer;
        this.parentId = parentId;
        this.categoryId = categoryId;
        this.orderInLesson = orderInLesson;
    }

    public Integer getLessonId() {
        return lessonId;
    }

    public void setLessonId(Integer lessonId) {
        this.lessonId = lessonId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public Float getMark() {
        return mark;
    }

    public void setMark(Float mark) {
        this.mark = mark;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getOrderInLesson() {
        return orderInLesson;
    }

    public void setOrderInLesson(Integer orderInLesson) {
        this.orderInLesson = orderInLesson;
    }

    @Override
    public String toString() {
        return
                "lessonId=" + lessonId +
                ", type='" + type + '\'' +
                ", description='" + description + '\'' + content +
                ", mark=" + mark +
                ", answer='" + answer + '\'' +
                ", parentId=" + parentId +
                ", categoryId=" + categoryId +
                ", orderInLesson=" + orderInLesson
                ;
    }
}
