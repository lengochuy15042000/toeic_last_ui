package com.lengochuy.dmt.uilasttoeic.Model;

import java.util.List;

//Integer lessonId, String type, String description,
//        String content, Float mark, String answer, Integer parentId,
//        Integer categoryId, Integer orderInLesson
public class QuestionLesson {
    private int resourceSound;
    private int resourceImage;
    private String textQuestion;
    private List<Answers> answersList;


    public QuestionLesson(int resourceSound, int resourceImage, String textQuestion, List<Answers> answersList) {
        this.resourceSound = resourceSound;
        this.resourceImage = resourceImage;
        this.textQuestion = textQuestion;
        this.answersList = answersList;
    }

    public int getResourceSound() {
        return resourceSound;
    }

    public void setResourceSound(int resourceSound) {
        this.resourceSound = resourceSound;
    }

    public int getResourceImage() {
        return resourceImage;
    }

    public void setResourceImage(int resourceImage) {
        this.resourceImage = resourceImage;
    }

    public String getTextQuestion() {
        return textQuestion;
    }

    public void setTextQuestion(String textQuestion) {
        this.textQuestion = textQuestion;
    }

    public List<Answers> getAnswersList() {
        return answersList;
    }

    public void setAnswersList(List<Answers> answersList) {
        this.answersList = answersList;
    }
}
