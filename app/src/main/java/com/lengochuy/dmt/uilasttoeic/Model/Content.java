package com.lengochuy.dmt.uilasttoeic.Model;

import java.util.List;

public class Content {
    private List<ListQuestion> questionList;
    private List<ListAnswers> answersList;
    private List<Explanation> explanationList;

    public Content(List<ListQuestion> questionList, List<ListAnswers> answersList,
                   List<Explanation> explanationList) {
        this.questionList = questionList;
        this.answersList = answersList;
        this.explanationList = explanationList;
    }

    public List<ListQuestion> getQuestionList() {
        return questionList;
    }

    public void setQuestionList(List<ListQuestion> questionList) {
        this.questionList = questionList;
    }

    public List<ListAnswers> getAnswersList() {
        return answersList;
    }

    public void setAnswersList(List<ListAnswers> answersList) {
        this.answersList = answersList;
    }

    public List<Explanation> getExplanationList() {
        return explanationList;
    }

    public void setExplanationList(List<Explanation> explanationList) {
        this.explanationList = explanationList;
    }

    @Override
    public String toString() {
        return "{" +
                "question" + ":["+ questionList + "],"+
                ", answers" + ":["+ answersList + "],"+
                ", explanation" + ":[" + explanationList + "]"+
                '}';
    }
}
