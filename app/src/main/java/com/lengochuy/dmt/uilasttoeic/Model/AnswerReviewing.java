package com.lengochuy.dmt.uilasttoeic.Model;

public class AnswerReviewing {
    private int indexAnswer;
    private boolean isTrue = true;

    public AnswerReviewing(int indexAnswer, boolean isTrue) {
        this.indexAnswer = indexAnswer;
        this.isTrue = isTrue;
    }

    public int getIndexAnswer() {
        return indexAnswer;
    }

    public void setIndexAnswer(int indexAnswer) {
        this.indexAnswer = indexAnswer;
    }

    public boolean isTrue() {
        return isTrue;
    }

    public void setTrue(boolean aTrue) {
        isTrue = aTrue;
    }
}
