package com.lengochuy.dmt.uilasttoeic.Model;

public class Answers {
    private String label, contentAnswer;
    private boolean isTrue;

    public Answers(String label, String contentAnswer, boolean isTrue) {
        this.label = label;
        this.contentAnswer = contentAnswer;
        this.isTrue = isTrue;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getContentAnswer() {
        return contentAnswer;
    }

    public void setContentAnswer(String contentAnswer) {
        this.contentAnswer = contentAnswer;
    }

    public boolean isTrue() {
        return isTrue;
    }

    public void setTrue(boolean aTrue) {
        isTrue = aTrue;
    }
}
